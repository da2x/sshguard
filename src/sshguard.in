#!/bin/sh
# sshguard -- protect hosts from brute-force attacks

libexec="@libexecdir@"
version="@sshguardversion@"

# Source configuration file
config="@sysconfdir@/sshguard.conf"
if [ ! -r $config ]; then
    echo "sshguard: Could not read '$config'" >&2
    echo "Please configure SSHGuard." >&2
    exit 78
fi

. $config

usage() {
    cat << EOF
Usage: sshguard [-v] [-h]
[-a BLACKLIST-THRESHOLD] [-b BLACKLIST-FILE]
[-i PID-FILE] [-p BLOCK_TIME]
[-s DETECTION_TIME] [-w IP-ADDRESS | WHITELIST-FILE]
EOF
}

# Override configuration options with runtime arguments
while getopts "b:l:p:s:a:w:i:hv" opt; do
    case $opt in
        a) THRESHOLD=$OPTARG;;
        b) BLACKLIST_FILE=$OPTARG;;
        i) PID_FILE=$OPTARG;;
        l) FILES="$FILES $OPTARG";;
        p) BLOCK_TIME=$OPTARG;;
        s) DETECTION_TIME=$OPTARG;;
        w) WHITELIST_ARG="$WHITELIST_ARG $OPTARG";;
        h) usage; exit;;
        v) echo "SSHGuard $version"; exit;;
    esac
done

# Read config in to flags
if [ ! -z "$THRESHOLD" ]; then
    flags="$flags -a $THRESHOLD"
fi
if [ ! -z "$BLACKLIST_FILE" ]; then
    flags="$flags -b $BLACKLIST_FILE"
fi
if [ ! -z "$PID_FILE" ]; then
    flags="$flags -i $PID_FILE"
fi
if [ ! -z "$BLOCK_TIME" ]; then
    flags="$flags -p $BLOCK_TIME"
fi
if [ ! -z "$DETECTION_TIME" ]; then
    flags="$flags -s $DETECTION_TIME"
fi
if [ ! -z "$WHITELIST_ARG" ]; then
    for arg in $WHITELIST_ARG; do
      flags="$flags -w $arg"
    done
elif [ ! -z "$WHITELIST_FILE" ]; then
    flags="$flags -w $WHITELIST_FILE"
fi

# Check backend
if [ -z "$BACKEND" ]; then
    echo "sshguard: BACKEND must be set in '$config'" >&2
    exit 78
elif [ ! -x "$BACKEND" ]; then
    echo "sshguard: '$BACKEND' is not executable" >&2
    exit 78
fi

# Log source selection order: runtime args, logreader, files, or stdin
shift $((OPTIND-1))
if [ ! -z "$@" ]; then
    tailcmd="$libexec/sshg-logtail $@"
elif [ ! -z "$LOGREADER" ]; then
    tailcmd="$LOGREADER"
elif [ ! -z "$FILES" ]; then
    tailcmd="$libexec/sshg-logtail $FILES"
elif [ -z "$tailcmd" ]; then
    echo "sshguard: Reading from stdin. You probably shouldn't be doing this." >&2
    tailcmd="cat"
fi

eval $tailcmd | $libexec/sshg-parser | \
    ($libexec/sshg-blocker $flags || kill -PIPE $$) | \
    ($BACKEND || kill -PIPE $$)
